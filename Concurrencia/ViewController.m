//
//  ViewController.m
//  Concurrencia
//
//  Created by Fernando Rodríguez Romero on 03/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)sincrono:(id)sender{
    
    NSURL *url = [NSURL URLWithString:@"http://img2.ruliweb.daum.net/mypi/gup/120/1772_2.jpg"];
    
    NSData *imgData = [NSData dataWithContentsOfURL:url];
    
    UIImage *img = [UIImage imageWithData:imgData];
    
    self.photoView.image = img;
    
    
    
}

-(IBAction)asincrono:(id)sender{
    
    // Creo una cola
    dispatch_queue_t gemelas = dispatch_queue_create("biz.agbo.concurrencia.davalos", 0);
    
    // le mando el bloque
    dispatch_async(gemelas, ^{
        
        // Ya estamos en segundo plano!!!
        NSURL *url = [NSURL URLWithString:@"http://img2.ruliweb.daum.net/mypi/gup/120/1772_2.jpg"];
        
        NSData *imgData = [NSData dataWithContentsOfURL:url];
        
        UIImage *img = [UIImage imageWithData:imgData];
        
        
        // Nos vamos al hilo principal a mostrar la gemela en
        // la imageView
        dispatch_async(dispatch_get_main_queue(), ^{
            self.photoView.image = img;
            
        });
        
    });
    


}

-(IBAction)asincronoPro:(id)sender{
    
    [self withImage:^(UIImage *image) {
       
        self.photoView.image = image;
    }];
}




-(void) withImage:(void (^)(UIImage*image))completionBlock{
    
    
    // Nos vamos a segundo plano
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        
        // Ya estamos en segundo plano!!!
        NSURL *url = [NSURL URLWithString:@"http://img2.ruliweb.daum.net/mypi/gup/120/1772_2.jpg"];
        
        NSData *imgData = [NSData dataWithContentsOfURL:url];
        
        UIImage *img = [UIImage imageWithData:imgData];

        
        // nos vamos a primer plano a ejecutar el bloque de
        // fianlización
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock(img);
        });
    });
}

















@end
