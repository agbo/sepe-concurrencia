//
//  ViewController.h
//  Concurrencia
//
//  Created by Fernando Rodríguez Romero on 03/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *photoView;

-(IBAction)sincrono:(id)sender;

-(IBAction)asincrono:(id)sender;

-(IBAction)asincronoPro:(id)sender;
@end

